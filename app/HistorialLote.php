<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialLote extends Model
{
    //
    public function lotes(){
        return $this->hasMany('App\Lote');
    }
}
