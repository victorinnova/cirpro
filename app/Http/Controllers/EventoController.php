<?php

namespace App\Http\Controllers;
use App\Evento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Evento $evento)
    {
        //
        $evento = Evento::all();
        return view();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $evento = new Evento;
        if($request->cambio != ''){
            $evento->cambio = $request->cambio;
        }else{
            $evento->cambio="";
        }
        if($request->descripcion != ''){
            $evento->descripcion = $request->descripcion;
        }else{
            $evento->descripcion="";
        }
        $evento->save();
        return view('admin.create_evento');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Evento $evento)
    {
        //
        $evento = Evento::create($request->all());
        return redirect('eventos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Evento $evento)
    {
        //
        $evento=Evento::find($id);
        return view('admin.update_evento')->with('evento',$ejercicio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Evento $evento)
    {
        //
        $evento=Evento::find($id);
        return view('admin.update_evento')->with('evento',$evento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Evento $evento)
    {
        //
        $evento = Evento::find($id);
        $eventos=Evento::all();
        if($request->cambio != ''){
            $evento->cambio = $request->cambio;
        }else{
            $evento->cambio="";
        }
        if($request->descripcion != ''){
            $evento->descripcion = $request->descripcion;
        }else{
            $evento->descripcion="";
        }
        DB::table('eventos')->where('id', $id)->update(['cambio' => $cambio, 'descripcion'=>$descripcion]);
        // echo $request;

        if($evento){
            return view('admin.list_ejercicio')->with('eventos',$eventos);
        }else{
            return view('admin.list_ejercicio')->with('eventos',$eventos);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Evento $evento)
    {
        //
        $eventos=Evento::all();
        $evento=Evento::find($id);
        $evento->delete();
        return view('admin.list_evento')->with('eventos',$eventos);
    }
    public function web($id){
        $evento=Evento::find($id);
        return view('masinfo')->with('evento',$evento);
    }
}
