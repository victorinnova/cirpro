<?php

namespace App\Http\Controllers;

use App\HistorialLote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HistorialLoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HistorialLote $historiallote)
    {
        //
        $historiallotes = HistorialLote::all();
        return view('admin.list_historiallote')->with('historiallotes',$historiallotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, HistorialLote $historiallote)
    {
        //
        $historiallote = HistorialLote::create($request->all());
        return redirect('');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, HistorialLote $historiallote)
    {
        //
        $historiallote=HistorialLote::find($id);
        return view('admin.update_ejercicio')->with('historiallote',$historiallote);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, HistorialLote $historiallote)
    {
        //
        $historiallote=HistorialLote::find($id);
        return view('admin.update_ejercicio')->with('historiallote',$historiallote);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HistorialLote $historiallote)
    {
        //
        $historiallotes=HistorialLote::all();
        $historiallote=HistorialLote::find($id);
        $historiallote->delete();
        return view('admin.list_historiallote')->with('historiallotes',$historiallotes);
    }
    public function web($id,HistorialLote $historiallote){
        $historiallote=HistorialLote::find($id);
        return view('masinfo')->with('historiallote',$historiallote);
    }
}
