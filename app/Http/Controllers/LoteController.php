<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lote;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Lote $lote)
    {
        //
        $lotes = Lote::all();
        return view('admin.list_ejercicio')->with('lotes',$lotes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Lote $lote)
    {
        //
        $lote = new Lote;
        if($request->nom != ''){
            $lote->piezastotales = $request->piezastotales;
        }else{
            $lote->piezastotales="";
        }
        if($request->numerolote != ''){
            $lote->numerolote = $request->numerolote;
        }else{
            $lote->numerolote="";

        }
        if($request->numeroof != ''){
            $lote->numeroof = $request->numeroof;
        }else{
            $lote->numeroof="0";
        }

        if($request->version != ''){
            $lote->version = $request->version;
        }else{
            $lote->$version ="";
        }
        if($request->loteactivo != ''){
            $lote->loteactivo = $request->loteactivo;
        }else{
            $lote->$loteactivo ="";
        }
        if($request->produccion != ''){
            $lote->produccion = $request->produccion;
        }else{
            $lote->$produccion ="";
        }
        if($request->codigocomponente != ''){
            $lote->codigocomponente = $request->codigocomponente;
        }else{
            $lote->$codigocomponente ="";
        }
        if($request->cantidadtotal != ''){
            $lote->cantidadtotal = $request->cantidadtotal;
        }else{
            $lote->$cantidadtotal ="";
        }
        if($request->cantidadreal != ''){
            $lote->cantidadreal = $request->cantidadreal;
        }else{
            $lote->$cantidadreal ="";
        }
        $lote->save();
        return view('admin.create_ejercicio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Lote $lote)
    {
        //
        $lote = Lote::create($request->all());
        return redirect('lotes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Lote $lote)
    {
        //
        $lote=Lote::find($id);
        return view('admin.update_ejercicio')->with('lote',$lote);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Lote $lote)
    {
        //
        $lote=Lote::find($id);
        return view('admin.update_lote')->with('lote',$lote);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Lote $lote)
    {
        //
        $user = Lote::find($id);
        $lotes= Lote::all();

        if($request->nom != ''){
            $lote->piezastotales = $request->piezastotales;
        }else{
            $lote->piezastotales="";
        }
        if($request->numerolote != ''){
            $lote->numerolote = $request->numerolote;
        }else{
            $lote->numerolote="";

        }
        if($request->numeroof != ''){
            $lote->numeroof = $request->numeroof;
        }else{
            $lote->numeroof="0";
        }

        if($request->version != ''){
            $lote->version = $request->version;
        }else{
            $lote->$version ="";
        }
        if($request->loteactivo != ''){
            $lote->loteactivo = $request->loteactivo;
        }else{
            $lote->$loteactivo ="";
        }
        if($request->produccion != ''){
            $lote->produccion = $request->produccion;
        }else{
            $lote->$produccion ="";
        }
        if($request->codigocomponente != ''){
            $lote->codigocomponente = $request->codigocomponente;
        }else{
            $lote->$codigocomponente ="";
        }
        if($request->cantidadtotal != ''){
            $lote->cantidadtotal = $request->cantidadtotal;
        }else{
            $lote->$cantidadtotal ="";
        }
        if($request->cantidadreal != ''){
            $lote->cantidadreal = $request->cantidadreal;
        }else{
            $lote->$cantidadreal ="";
        }
        DB::table('lotes')->where('id', $id)->update(['piezastotales' => $piezastotales, 'numerolote'=>$numerolote,'numeroof'=>$numeroof, 'version'=>$version,  'loteactivo'=>$loteactivo,'produccion'=>$produccion,'codigocomponente'=>$codigocomponente,'cantidadtotal'=>$cantidadtotal,'cantidadreal'=>$cantidadreal, ]);
        // echo $request;

        if($user){
            return view('admin.list_ejercicio')->with('lotes',$lotes);
        }else{
            return view('admin.list_ejercicio')->with('lotes',$lotes);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Lote $lote)
    {
        //
        $lotes=Lote::all();
        $lote=Lote::find($id);
        $lote->delete();
        return view('admin.list_lote')->with('lotes',$lotes);
    }
    public function web($id){
        $lote=Lote::find($id);
        return view('masinfo')->with('lote',$lote);
    }
}
