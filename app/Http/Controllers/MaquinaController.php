<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maquina;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MaquinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Maquina $maquina)
    {
        //
        $maquinas = maquina::all();
        return view('welcome')->with('maquinas',$maquinas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Maquina $maquina)
    {
        //
        $maquina = new Maquina;
        if($request->codigo != ''){
            $maquina->codigo = $request->codigo;
        }else{
            $maquina->codigo="";
        }
        $maquina->save();
        return view('admin.creat_maquina');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Maquina $maquina)
    {
        //
        $maquina = Maquina::create($request->all());
        return redirect('maquinas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Maquina $maquina)
    {
        //
        $maquina=Maquina::find($id);
        return view('admin.update_ejercicio')->with('maquina',$maquina);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Maquina $maquina)
    {
        //
        $maquina=Maquina::find($id);
        return view('admin.update_ejercicio')->with('maquina',$maquina);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Maquina $maquina)
    {
        //
        $user = Maquina::find($id);
        $maquinas=Maquina::all();
        if($request->codigo != ''){
            $maquina->codigo = $request->codigo;
        }else{
            $maquina->codigo="";
        }
        DB::table('maquinas')->where('id', $id)->update(['codigo' => $codigo]);
        // echo $request;

        if($user){
            return view('admin.list_ejercicio')->with('maquinas',$maquinas);
        }else{
            return view('admin.list_ejercicio')->with('maquinas',$maquinas);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Maquina $maquina)
    {
        //
        $maquinas=Maquina::all();
        $maquina=Maquina::find($id);
        $maquina->delete();
        return view('admin.list_maquina')->with('maquinas',$maquinas);
    }
    public function web($id){
        $maquina=maquina::find($id);
        return view('masinfo')->with('maquina',$maquina);
    }
}
