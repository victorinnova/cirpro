<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Master;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Master $masters)
    {
        //
        $masters = Master::all();
        return view('admin.list_master')->with('masters',$masters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $master = new Master;
        if($request->descartucho != ''){
            $master->descartucho = $request->descartucho;
        }else{
            $master->descartucho="";
        }
        if($request->refinteriorcartucho != null){
            $master->refinteriorcartucho = $request->refinteriorcartucho;
        }else{
            $master->refinteriorcartucho=null;

        }
        if($request->refvaristor != null){
            $master->refvaristor = $request->refvaristor;
        }else{
            $master->refvaristor=null;
        }

        if($request->refflejen != null){
            $master->refflejen = $request->refflejen;
        }else{
            $master->$refflejen =null;
        }
        if($request->refmuelle != null){
            $master->refmuelle = $request->refmuelle;
        }else{
            $master->$refmuelle =null;
        }
        if($request->refdeslizador != null){
            $master->refdeslizador = $request->refdeslizador;
        }else{
            $master->$refdeslizador =null;
        }
        if($request->refindicador != null){
            $master->refindicador = $request->refindicador;
        }else{
            $master->$refindicador =null;
        }
        if($request->refselecttension != null){
            $master->refselecttension = $request->refselecttension;
        }else{
            $master->$refselecttension =null;
        }
        if($request->refcremaestano != null){
            $master->refcremaestano = $request->refcremaestano;
        }else{
            $master->$refcremaestano =null;
        }
        if($request->refadesivodelo != null){
            $master->refadesivodelo = $request->refadesivodelo;
        }else{
            $master->$refadesivodelo =null;
        }
        if($request->inprogmarcajelaser != null){
            $master->inprogmarcajelaser = $request->inprogmarcajelaser;
        }else{
            $master->$inprogmarcajelaser =null;
        }
        if($request->inposselectortension != null){
            $master->inposselectortension = $request->inposselectortension;
        }else{
            $master->$inposselectortension =null;
        }
        if($request->inprogsoldn != null){
            $master->inprogsoldn = $request->inprogsoldn;
        }else{
            $master->$inprogsoldn =null;
        }
        if($request->inprogdossn != null){
            $master->inprogdossn = $request->inprogdossn;
        }else{
            $master->$inprogdossn =null;
        }
        if($request->inprogsoldl != null){
            $master->inprogsoldl = $request->inprogsoldl;
        }else{
            $master->$inprogsoldl =null;
        }
        if($request->breakdownmax != null){
            $master->breakdownmax = $request->breakdownmax;
        }else{
            $master->$breakdownmax =null;
        }
        if($request->breakdownmin != null){
            $master->breakdownmin = $request->breakdownmin;
        }else{
            $master->$breakdownmin =null;
        }
        if($request->inprogposdelosdos != null){
            $master->inprogposdelosdos = $request->inprogposdelosdos;
        }else{
            $master->$inprogposdelosdos =null;
        }
        $master->save();
        return view('admin.create_master');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Master $master)
    {
        //
        $master = Master::create($request->all());
        return redirect('masters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Master $master)
    {
        //
        $master=Master::find($id);
        return view('admin.update_master')->with('master',$master);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Master $master)
    {
        //
        $master=Master::find($id);
        return view('admin.update_master')->with('master',$master);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,master $master)
    {
        //
       
        $user = Master::find($id);
        $masters=Master::all();
        // echo $request;
        // $user->update($request->all());

        if($request->descartucho != ''){
            $master->descartucho = $request->descartucho;
        }else{
            $master->descartucho="";
        }
        if($request->refinteriorcartucho != null){
            $master->refinteriorcartucho = $request->refinteriorcartucho;
        }else{
            $master->refinteriorcartucho=null;

        }
        if($request->refvaristor != null){
            $master->refvaristor = $request->refvaristor;
        }else{
            $master->refvaristor=null;
        }

        if($request->refflejen != null){
            $master->refflejen = $request->refflejen;
        }else{
            $master->$refflejen =null;
        }
        if($request->refmuelle != null){
            $master->refmuelle = $request->refmuelle;
        }else{
            $master->$refmuelle =null;
        }
        if($request->refdeslizador != null){
            $master->refdeslizador = $request->refdeslizador;
        }else{
            $master->$refdeslizador =null;
        }
        if($request->refindicador != null){
            $master->refindicador = $request->refindicador;
        }else{
            $master->$refindicador =null;
        }
        if($request->refselecttension != null){
            $master->refselecttension = $request->refselecttension;
        }else{
            $master->$refselecttension =null;
        }
        if($request->refcremaestano != null){
            $master->refcremaestano = $request->refcremaestano;
        }else{
            $master->$refcremaestano =null;
        }
        if($request->refadesivodelo != null){
            $master->refadesivodelo = $request->refadesivodelo;
        }else{
            $master->$refadesivodelo =null;
        }
        if($request->inprogmarcajelaser != null){
            $master->inprogmarcajelaser = $request->inprogmarcajelaser;
        }else{
            $master->$inprogmarcajelaser =null;
        }
        if($request->inposselectortension != null){
            $master->inposselectortension = $request->inposselectortension;
        }else{
            $master->$inposselectortension =null;
        }
        if($request->inprogsoldn != null){
            $master->inprogsoldn = $request->inprogsoldn;
        }else{
            $master->$inprogsoldn =null;
        }
        if($request->inprogdossn != null){
            $master->inprogdossn = $request->inprogdossn;
        }else{
            $master->$inprogdossn =null;
        }
        if($request->inprogsoldl != null){
            $master->inprogsoldl = $request->inprogsoldl;
        }else{
            $master->$inprogsoldl =null;
        }
        if($request->breakdownmax != null){
            $master->breakdownmax = $request->breakdownmax;
        }else{
            $master->$breakdownmax =null;
        }
        if($request->breakdownmin != null){
            $master->breakdownmin = $request->breakdownmin;
        }else{
            $master->$breakdownmin =null;
        }
        if($request->inprogposdelosdos != null){
            $master->inprogposdelosdos = $request->inprogposdelosdos;
        }else{
            $master->$inprogposdelosdos =null;
        }

        DB::table('masters')->where('id', $id)->update(['descartucho' => $descartucho, 'refinteriorcartucho'=>$refinteriorcartucho,'refvatistor'=>$refvatistor, 'refflejen'=>$refflejen,'refmuelle'=>$refmuelle, 'refdeslizador'=>$refdeslizador, 'refindicador'=>$refindicador, 'refselecttension'=>$refselecttension, 'refcremaestano'=>$refcremaestano, 'refadesivodalo'=>$refadesivodalo, 'inprogmarcajelaser'=>$inprogmarcajelaser, 'inprogsoldn'=>$inprogsoldn, 'inprogdossn'=>$inprogdossn, 'inprogsoldl'=>$inprogsoldl, 'breakdownmax'=>$breakdownmax, 'breakdownmin'=>$breakdownmin, 'inprogposdelodos'=>$inprogposdelodos]);
        // echo $request;

        if($user){
            return view('admin.list_master')->with('masters',$masters);
        }else{
            return view('admin.list_master')->with('usuarios',$masters);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Master $master)
    {
        //
        $masters=Master::all();
        $master=Master::find($id);
        $master->delete();
        return view('admin.list_master')->with('masters',$masters);
    }
    public function web($id){
        $master=Master::find($id);
        return view('masinfo')->with('master',$master);
    }
}

