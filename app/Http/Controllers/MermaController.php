<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mermas;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class MermaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Merma $mermas)
    {
        //
        $mermas = Merma::all();
        return view('admin.list_merma')->with('mermas',$mermas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $merma = new Merma;
        if($request->motivo != ''){
            $merma->motivo = $request->motivo;
        }else{
            $merma->motivo="";
        }
        if($request->cdm != ''){
            $merma->cdm = $request->cdm;
        }else{
            $merma->cdm="";

        }
        if($request->descripcion != ''){
            $merma->descripcion = $request->descripcion;
        }else{
            $merma->descripcion="";
        }

        
        $merma->save();
        return view('admin.create_merma');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Merma $merma)
    {
        //
        $merma = Merma::create($request->all());
        return redirect('mermas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Merma $merma)
    {
        //
        $merma=Merma::find($id);
        return view('admin.update_merma')->with('merma',$merma);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Merma $merma)
    {
        //
        $merma=Merma::find($id);
        return view('admin.update_merma')->with('merma',$merma);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,Merma $merma)
    {
        //
       
        $user = Merma::find($id);
        $mermas=Merma::all();
        // echo $request;
        // $user->update($request->all());
        if($request->motivo != ''){
            $merma->motivo = $request->motivo;
        }else{
            $merma->motivo="";
        }
        if($request->cdm != ''){
            $merma->cdm = $request->cdm;
        }else{
            $merma->cdm="";

        }
        if($request->descripcion != ''){
            $merma->descripcion = $request->descripcion;
        }else{
            $merma->descripcion="";
        }

        DB::table('mermas')->where('id', $id)->update(['motivo' => $nom, 'cdm'=>$cdm,'descripcion'=>$descripcion]);
        // echo $request;

        if($user){
            return view('admin.list_merma')->with('mermas',$mermas);
        }else{
            return view('admin.list_merma')->with('usuarios',$mermas);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Merma $merma)
    {
        //
        $mermas=Merma::all();
        $merma=Merma::find($id);
        $merma->delete();
        return view('admin.list_merma')->with('mermas',$mermas);
    }
    public function web($id){
        $merma=Merma::find($id);
        return view('masinfo')->with('merma',$merma);
    }
}
