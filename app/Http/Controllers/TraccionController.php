<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traccion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class TraccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Traccion $traccions)
    {
        //
        $traccions = Traccion::all();
        return view('admin.list_traccion')->with('traccions',$traccions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $traccion = new Traccion;
        if($request->valor != null){
            $traccion->valor = $request->valor;
        }else{
            $traccion->valor=null;
        }
        if($request->cdm != ''){
            $traccion->cdm = $request->cdm;
        }else{
            $traccion->cdm="";

        }
        if($request->descripcion != ''){
            $traccion->descripcion = $request->descripcion;
        }else{
            $traccion->descripcion="";
        }

        
        $traccion->save();
        return view('admin.create_traccion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Traccion $traccion)
    {
        //
        $traccion = Traccion::create($request->all());
        return redirect('traccions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Traccion $traccion)
    {
        //
        $traccion=Traccion::find($id);
        return view('admin.update_traccion')->with('traccion',$traccion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Traccion $traccion)
    {
        //
        $traccion=Traccion::find($id);
        return view('admin.update_traccion')->with('traccion',$traccion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,Traccion $traccion)
    {
        //
       
        $user = Traccion::find($id);
        $traccions=Traccion::all();
        // echo $request;
        // $user->update($request->all());
        if($request->valor != ''){
            $traccion->valor = $request->valor;
        }else{
            $traccion->valor="";
        }
        if($request->cdm != ''){
            $traccion->cdm = $request->cdm;
        }else{
            $traccion->cdm="";

        }
        if($request->descripcion != ''){
            $traccion->descripcion = $request->descripcion;
        }else{
            $traccion->descripcion="";
        }

        DB::table('traccions')->where('id', $id)->update(['valor' => $nom, 'cdm'=>$cdm,'descripcion'=>$descripcion]);
        // echo $request;

        if($user){
            return view('admin.list_traccion')->with('traccions',$traccions);
        }else{
            return view('admin.list_traccion')->with('usuarios',$traccions);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Traccion $traccion)
    {
        //
        $traccions=Traccion::all();
        $traccion=Traccion::find($id);
        $traccion->delete();
        return view('admin.list_traccion')->with('traccions',$traccions);
    }
    public function web($id){
        $traccion=Traccion::find($id);
        return view('masinfo')->with('traccion',$traccion);
    }
}
