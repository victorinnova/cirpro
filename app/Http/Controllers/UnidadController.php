<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unidad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UnidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Unidad $unidads)
    {
        //
        $unidads = Unidad::all();
        return view('admin.list_unidad')->with('unidads',$unidads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $unidad = new Unidad;
        if($request->cmd != ''){
            $unidad->cmd = $request->cmd;
        }else{
            $unidad->cmd="";
        }
        if($request->info1 != ''){
            $unidad->info1 = $request->info1;
        }else{
            $unidad->info1="";

        }
        if($request->info2 != ''){
            $unidad->info2 = $request->info2;
        }else{
            $unidad->info2="";

        }
        if($request->info3 != ''){
            $unidad->info3 = $request->info3;
        }else{
            $unidad->info3="";

        }
        if($request->info4 != ''){
            $unidad->info4 = $request->info4;
        }else{
            $unidad->info4="";

        }
        if($request->info5 != ''){
            $unidad->info5 = $request->info5;
        }else{
            $unidad->info5="";

        }
        if($request->info6 != ''){
            $unidad->info6 = $request->info6;
        }else{
            $unidad->info6="";
        }
        if($request->info7 != ''){
            $unidad->info7 = $request->info7;
        }else{
            $unidad->info7="";
        }
        if($request->info8 != ''){
            $unidad->info8 = $request->info8;
        }else{
            $unidad->info8="";
        }
        if($request->info9 != ''){
            $unidad->info9 = $request->info9;
        }else{
            $unidad->info9="";
        }
        if($request->info10 != ''){
            $unidad->info10 = $request->info10;
        }else{
            $unidad->info10="";
        }
        $unidad->save();
        return view('admin.create_unidad');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Unidad $unidad)
    {
        //
        $unidad = Unidad::create($request->all());
        return redirect('unidads');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Unidad $unidad)
    {
        //
        $unidad=Unidad::find($id);
        return view('admin.update_unidad')->with('unidad',$unidad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Unidad $unidad)
    {
        //
        $unidad=Unidad::find($id);
        return view('admin.update_unidad')->with('unidad',$unidad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,Unidad $unidad)
    {
        //
       
        $user = Unidad::find($id);
        $unidads=Unidad::all();
        // echo $request;
        // $user->update($request->all());

        if($request->cmd != null){
            $unidad->cmd = $request->cmd;
        }else{
            $unidad->cmd=null;
        }
        if($request->info1 != ''){
            $unidad->info1 = $request->info1;
        }else{
            $unidad->info1="";

        }
        if($request->info2 != ''){
            $unidad->info2 = $request->info2;
        }else{
            $unidad->info2="";

        }
        if($request->info3 != ''){
            $unidad->info3 = $request->info3;
        }else{
            $unidad->info3="";

        }
        if($request->info4 != ''){
            $unidad->info4 = $request->info4;
        }else{
            $unidad->info4="";

        }
        if($request->info5 != ''){
            $unidad->info5 = $request->info5;
        }else{
            $unidad->info5="";

        }
        if($request->info6 != ''){
            $unidad->info6 = $request->info6;
        }else{
            $unidad->info6="";
        }
        if($request->info7 != ''){
            $unidad->info7 = $request->info7;
        }else{
            $unidad->info7="";
        }
        if($request->info8 != ''){
            $unidad->info8 = $request->info8;
        }else{
            $unidad->info8="";
        }
        if($request->info9 != ''){
            $unidad->info9 = $request->info9;
        }else{
            $unidad->info9="";
        }
        if($request->info10 != ''){
            $unidad->info10 = $request->info10;
        }else{
            $unidad->info10="";
        }

        DB::table('unidads')->where('id', $id)->update(['nom' => $nom, 'descripcion'=>$descripcion,'porcentajedificultad'=>$porcentajedificultad, 'url'=>$url,  'zona'=>$zona]);
        // echo $request;

        if($user){
            return view('admin.list_unidad')->with('unidads',$unidads);
        }else{
            return view('admin.list_unidad')->with('usuarios',$unidads);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Unidad $unidad)
    {
        //
        $unidads=Unidad::all();
        $unidad=Unidad::find($id);
        $unidad->delete();
        return view('admin.list_unidad')->with('unidads',$unidads);
    }
    public function web($id){
        $unidad=Unidad::find($id);
        return view('masinfo')->with('unidad',$unidad);
    }
}
