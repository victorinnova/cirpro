<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserCustomController extends Controller
{

    public function index()
    {
        return User::all();
    }

    public function showAll(){
        if(auth()->user()->role == 1){
            $usuarios = User::all();
            return view('admin.list_users')->with('usuarios', $usuarios);
        }
        else{
            return view('welcome');
        }
    }

    public function create(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $user = new User;
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->save();
        return response()->json(null,204);
        
    }
        public function create_web(Request $request)
    {
        // echo $request;
        $usuari = User::create($request->all());
        $usuaris=User::all();
        return view('admin.list_users')->with('usuarios',$usuaris);

        
    }

    public function store(Request $request)
    {
        //
    }

    public function show_web($id){

        $user = User::find($id);
        return view('admin/update_users')->with('user', $user);

    }

    public function show($id)
    {
        $user = User::find($id);
        return view('usuario_profile')->with('user',$user);
    }
    function update(Request $request, $id){
        $user = User::find($id);

        if($request->email != ''){
            $email = $request->email;
        }else{
            $email="";
        }
        if($request->bloqueado != ''){
            $bloqueado = $request->bloqueado;
        }else{
            $bloqueado=false;

        }
        if($request->name != ''){
            $name = $request->name;
        }else{
            $name="0";
        }

        if($request->password != ''){
            $password = $request->password;
        }else{
            $password ="";
        }
        if($request->apellido != ''){
            $apellido = $request->apellido;
        }else{
            $apellido ="";
        }
        if($request->codigo != ''){
            $codigo = $request->codigo;
        }else{
            $codigo ="";
        }
        if($request->rol != ''){
            $rol = $request->rol;
        }else{
            $rol =null;
        }

        DB::table('users')->where('id', $id)->update(['name' => $name, 'apellido'=>$apellido,'email'=>$email, 'bloqueado'=>$bloqueado,  'password'=>$password,'codigo'=>$codigo,'rol'=>$rol]);
        // echo $request;

        if($user){
            return view('usuario_profile')->with('user',$user);
        }else{
            return view('usuario_profile')->with('user',$user);
        }
    }
    function update_web(Request $request, $id){

        $user = User::find($id);
        $usuaris=User::all();
        // echo $request;
        // $user->update($request->all());

        if($request->email != ''){
            $email = $request->email;
        }else{
            $email="";
        }
        if($request->bloqueado != ''){
            $bloqueado = $request->bloqueado;
        }else{
            $bloqueado=false;

        }
        if($request->name != ''){
            $name = $request->name;
        }else{
            $name="0";
        }

        if($request->password != ''){
            $password = $request->password;
        }else{
            $password ="";
        }
        if($request->apellido != ''){
            $apellido = $request->apellido;
        }else{
            $apellido ="";
        }
        if($request->codigo != ''){
            $codigo = $request->codigo;
        }else{
            $codigo ="";
        }
        if($request->rol != ''){
            $rol = $request->rol;
        }else{
            $rol =null;
        }

        DB::table('users')->where('id', $id)->update(['name' => $name, 'apellido'=>$apellido,'email'=>$email, 'bloquejat'=>$bloquejat,  'password'=>$password,'edad'=>$edad,'peso'=>$peso,'altura'=>$altura,'imc'=>$imc,'is_admin'=>$is_admin]);
        // echo $request;

        if($user){
            return view('admin.list_users')->with('usuarios',$usuaris);
        }else{
            return view('admin.list_users')->with('usuarios',$usuaris);
        }
    }

    public function toggleDisable(Request $request)
    {

        $option = $request->disabled;
        $user = $request->user;
        DB::table('users')->where('id', $user)->update(['bloqueado' => $option]);

        $usuarios = User::all();
        return view('admin.list_users')->with('usuarios', $usuarios);
    }

    public function destroy($id)
    {
        // funciona de puta madre
        $user = User::find($id);
        $usuarios = User::all();
        $user->delete();
        return view('admin.list_users')->with('usuarios', $usuarios);
    }
    public function home($id){
        return view('home')->with('usuario', $usuario);
    }
}
