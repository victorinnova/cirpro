<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
    //
    protected $fillable=['piezastotales','numerolote','numeroof','numeroarticulo','version','loteactivo','produccion','codigocomponente','cantidadtotal','cantidadreal'];

    public function eventos(){
        return $this->hasMany('App\Evento');
    }
    public function mermas(){
        return $this->hasMany('App\Merma');
    }
    public function tracciones(){
        return $this->hasMany('App\Traccion');
    }
    public function unidads(){
        return $this->hasMany('App\unidad');
    }
}
