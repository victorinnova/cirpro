<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maquina extends Model
{
    //
    public function lotes(){
        return $this->hasMany('App\Lote');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
}
