<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    //
    protected $fillable=['desccartucho','refinteriorcartucho','refvaristor','refflejen','refflejel','refmuelle','refdeslizador','refindicador','refselecttension','refcremaestano','refadesivodelo','inprogmarcajelaser','inposselectortension','inprogsoldn','inprogdossn','inprogsoldl','breakdownmax','breakdownmin','inprogposdelodos'];
    public function lotes(){
        return $this->hasMany('App\Lote');
    }
}
