<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traccion extends Model
{
    //
    protected $fillable=['cmd','valor','descripcion'];
}
