<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desccartucho')->default('');
            $table->integer('refinteriorcartucho')->default(0);
            $table->integer('refvaristor')->default(0);
            $table->integer('refflejen')->default(0);
            $table->integer('refflejel')->default(0);
            $table->integer('refmuelle')->default(0);
            $table->integer('refdeslizador')->default(0);
            $table->integer('refindicador')->default(0);
            $table->integer('refselecttension')->default(0);
            $table->integer('refcremaestano')->default(0);
            $table->integer('refadesivodelo')->default(0);
            $table->integer('inprogmarcajelaser')->default(0);
            $table->integer('inposselectortension')->default(0);
            $table->integer('inprogsoldn')->default(0);
            $table->integer('inprogdossn')->default(0);
            $table->integer('inprogsoldl')->default(0);
            $table->integer('breakdownmax')->default(0);
            $table->integer('breakdownmin')->default(0);
            $table->integer('inprogposdelodos')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
    }
}
