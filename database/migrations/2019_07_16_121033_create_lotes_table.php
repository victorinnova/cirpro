<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('piezastotales');
            $table->string('numerolote')->default('');
            $table->string('numeroof')->default('');
            $table->string('numeroarticulo')->default('');
            $table->double('version')->default(0);
            $table->boolean('loteactivo')->default(true);
            $table->integer('produccion')->default(0);
            $table->string('codigocomponente')->default('');
            $table->integer('cantidadtotal')->default(0);
            $table->integer('cantidadreal')->default(0);
            $table->timestamps();
            $table->integer('master_id')->unsigned()->default(0);
            $table->foreign('master_id')->references('id')->on('masters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
