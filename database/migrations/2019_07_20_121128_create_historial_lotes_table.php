<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_lotes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('evento_id')->unsigned()->default(0);
            $table->foreign('evento_id')->references('id')->on('eventos')->onDelete('cascade');
            $table->integer('merma_id')->unsigned()->default(0);
            $table->foreign('merma_id')->references('id')->on('mermas')->onDelete('cascade');
            $table->integer('traccion_id')->unsigned()->default(0);
            $table->foreign('traccion_id')->references('id')->on('traccions')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('lote_id')->unsigned()->default(0);
            $table->foreign('lote_id')->references('id')->on('lotes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_lotes');
    }
}
