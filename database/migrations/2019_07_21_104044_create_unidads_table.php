<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cmd');
            $table->string('info1');
            $table->string('info2');
            $table->string('info3');
            $table->string('info4');
            $table->string('info5');
            $table->string('info6');
            $table->string('info7');
            $table->string('info8');
            $table->string('info9');
            $table->string('info10');
            $table->integer('lote_id')->unsigned()->default(0);
            $table->foreign('lote_id')->references('id')->on('lotes')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidads');
    }
}
